package dev.ceulemans.windesheim.wip.exception;

/**
 * Thrown when a user supplies an out-of-range number (less than 3 or more than 8) for dice generation.
 */
public class DiceGenerationException extends RuntimeException {
}
