package dev.ceulemans.windesheim.wip.exception;

/**
 * This exception is thrown when a die image could not be found.
 */
public class DiceImageException extends RuntimeException {
}
