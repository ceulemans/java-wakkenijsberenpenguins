package dev.ceulemans.windesheim.wip.converter;

import javafx.util.StringConverter;

public class StringToIntegerConverter extends StringConverter<Integer> {

    @Override
    public String toString(Integer integer) {
        if (integer == null) return null;
        return integer.toString();
    }

    @Override
    public Integer fromString(String s) {
        try {
            return Integer.parseUnsignedInt(s);
        } catch (NumberFormatException e) {
            return null;
        }
    }
}
