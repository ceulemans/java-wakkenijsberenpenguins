package dev.ceulemans.windesheim.wip.controller;

import dev.ceulemans.windesheim.wip.exception.DiceGenerationException;
import dev.ceulemans.windesheim.wip.exception.DiceImageException;
import dev.ceulemans.windesheim.wip.model.Die;
import dev.ceulemans.windesheim.wip.model.GameStore;
import dev.ceulemans.windesheim.wip.service.DiceGenerator;
import dev.ceulemans.windesheim.wip.service.IDiceGenerator;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the main controller of the application, called by the <code>classpath:/ui/application.fxml</code>.
 */
public class AppBaseController extends BaseController {

    private static final Logger logger = LoggerFactory.getLogger(AppBaseController.class);

    private final IDiceGenerator service = new DiceGenerator();
    private final GameStore store = new GameStore();

    @FXML
    private Slider input;
    @FXML
    private Button play;
    @FXML
    private FlowPane dice;
    @FXML
    private TextField holes;
    @FXML
    private TextField bears;
    @FXML
    private TextField penguins;
    @FXML
    private Button submit;

    /**
     * This method is called by JavaFX to initialize the UI components.
     */
    public void initialize() {
        TextFormatter<Integer> holesFormatter = setIntegerFormatter(holes);
        TextFormatter<Integer> bearsFormatter = setIntegerFormatter(bears);
        TextFormatter<Integer> penguinsFormatter = setIntegerFormatter(penguins);

        Runnable submitRunnable = () -> submitInputs(
                holesFormatter.getValue(),
                bearsFormatter.getValue(),
                penguinsFormatter.getValue()
        );

        EventHandler<KeyEvent> inputSubmitHandler = event -> {
            if (!KeyCode.ENTER.equals(event.getCode())) return;
            submitRunnable.run();
        };

        play.setOnAction(e -> handlePlayButtonClick());
        holes.setOnKeyPressed(inputSubmitHandler);
        bears.setOnKeyPressed(inputSubmitHandler);
        penguins.setOnKeyPressed(inputSubmitHandler);
        submit.setOnAction(e -> submitRunnable.run());

        play.fire();
    }

    private void printResult(GameStore result) {
        logger.debug("DiceResults(holes={}, bears={}, penguins={})", result.getHoles(), result.getBears(), result.getPenguins());
    }

    private void submitInputs(Integer holes, Integer bears, Integer penguins) {
        if (holes == null) holes = 0;
        if (bears == null) bears = 0;
        if (penguins == null) penguins = 0;

        boolean correct = store.checkAnswers(holes, bears, penguins);
        logger.debug("Answer(correct={})", correct);

        if (correct) {
            handlePlayButtonClick();
            createAlert("Je hebt het goede antwoord gegeven!").show();
            return;
        }

        store.incrementTries();

        if (store.getTries() == 3) {
            createAlert("Op de hoeken van een wak zitten ijsberen...").show();
        } else if (store.getTries() >= 8) {
            createAlert("Je hebt teveel pogingen gedaan, de dobbelstenen worden opnieuw gegenereerd...").show();
            handlePlayButtonClick();
        }
    }

    /**
     * Handle the "play" button click. This will generate <code>amount</code> of dice.
     */
    private void handlePlayButtonClick() {
        try {
            if (!dice.isManaged()) dice.setManaged(true);
            if (!dice.isVisible()) dice.setVisible(true);
            if (dice.getChildren().size() > 0) dice.getChildren().clear();
            resetInputs();

            int amount = (int) input.getValue();
            store.reset();
            store.setDice(service.generate(amount));

            for (Die die : store.getDice()) {
                ImageView image = getDieImage(die);
                dice.getChildren().add(image);
            }

            new Thread(() -> printResult(store)).start();

            holes.requestFocus();
        } catch (DiceGenerationException e) {
            logger.error("Error during dice generation", e);
        }
    }

    private void resetInputs() {
        if (!holes.getText().isBlank()) holes.clear();
        if (!bears.getText().isBlank()) bears.clear();
        if (!penguins.getText().isBlank()) penguins.clear();
    }

    private ImageView getDieImage(Die die) {
        if (die == null) throw new DiceImageException();
        ImageView view = new ImageView(die.getImage());
        view.setFitHeight(100);
        view.setFitWidth(100);
        return view;
    }

    private Alert createAlert(String message) {
        var alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText(null);
        alert.setContentText(message);
        return alert;
    }
}
