package dev.ceulemans.windesheim.wip.controller;

import dev.ceulemans.windesheim.wip.converter.StringToIntegerConverter;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.TextInputControl;
import javafx.util.StringConverter;

import java.util.function.UnaryOperator;

public abstract class BaseController {

    /**
     * Apply a string to integer converter to a certain text input field.
     *
     * @param control the control field
     * @return the text formatter, this is used to get the integer value
     */
    protected TextFormatter<Integer> setIntegerFormatter(TextInputControl control) {
        StringConverter<Integer> converter = new StringToIntegerConverter();
        UnaryOperator<TextFormatter.Change> filter = (change) -> {
            String text = change.getControlNewText();
            if (text.isBlank()) return change;
            try {
                Integer.parseInt(text);
                return change;
            } catch (NumberFormatException e) {
                return null;
            }
        };
        TextFormatter<Integer> formatter = new TextFormatter<>(converter, null, filter);
        control.setTextFormatter(formatter);
        return formatter;
    }
}
