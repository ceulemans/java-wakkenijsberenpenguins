package dev.ceulemans.windesheim.wip.model;

import dev.ceulemans.windesheim.wip.exception.DiceImageException;
import javafx.scene.image.Image;

import java.io.InputStream;

public enum Die {
    ONE("/dice/one.png", 1, 0, 6),
    TWO("/dice/two.png", 0, 0, 5),
    THREE("/dice/three.png", 1, 2, 4),
    FOUR("/dice/four.png", 0, 0, 3),
    FIVE("/dice/five.png", 1, 4, 2),
    SIX("/dice/six.png", 0, 0, 1);

    private final String image;
    private final int holes;
    private final int bears;
    private final int penguins;

    /**
     * Constructor for the Die enum.
     *
     * @param image    the path to the image used for this die's side
     * @param holes    the amount of holes this die has
     * @param bears    the amount of bears this die has
     * @param penguins the amount of penguins this die has
     */
    Die(String image, int holes, int bears, int penguins) {
        this.image = image;
        this.holes = holes;
        this.bears = bears;
        this.penguins = penguins;
    }

    public String getImageName() {
        return image;
    }

    /**
     * Get the image object from this enum.
     *
     * @return the image object
     * @throws DiceImageException when an image could not be found
     */
    public Image getImage() throws DiceImageException {
        InputStream resource = getClass().getResourceAsStream(getImageName());
        if (resource == null) {
            throw new DiceImageException();
        }
        return new Image(resource);
    }

    public int getHoles() {
        return holes;
    }

    public int getBears() {
        return bears;
    }

    public int getPenguins() {
        return penguins;
    }

    @Override
    public String toString() {
        return "Dice(name=" + name() + ')';
    }
}
