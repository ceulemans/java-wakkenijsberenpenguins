package dev.ceulemans.windesheim.wip.model;

import java.util.List;

public class GameStore {

    private DiceResult dice;
    private int tries = 0;

    public boolean checkAnswers(int holes, int bears, int penguins) {
        return holes == getHoles() && bears == getBears() && penguins == getPenguins();
    }

    public void reset() {
        this.tries = 0;
    }

    public void incrementTries() {
        this.tries++;
    }

    public List<Die> getDice() {
        return dice.getDice();
    }

    public void setDice(DiceResult dice) {
        this.dice = dice;
    }

    public int getHoles() {
        return dice.getHoles();
    }

    public int getBears() {
        return dice.getBears();
    }

    public int getPenguins() {
        return dice.getPenguins();
    }

    public int getTries() {
        return tries;
    }
}
