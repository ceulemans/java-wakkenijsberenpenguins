package dev.ceulemans.windesheim.wip.model;

import java.util.List;

public class DiceResult {

    private final List<Die> dice;
    private int holes = 0;
    private int bears = 0;
    private int penguins = 0;

    private DiceResult(List<Die> dice) {
        this.dice = dice;

        for (Die die : dice) {
            holes += die.getHoles();
            bears += die.getBears();
            penguins += die.getPenguins();
        }
    }

    public static DiceResult of(List<Die> dice) {
        return new DiceResult(dice);
    }

    public List<Die> getDice() {
        return dice;
    }

    public int getHoles() {
        return holes;
    }

    public int getBears() {
        return bears;
    }

    public int getPenguins() {
        return penguins;
    }
}
