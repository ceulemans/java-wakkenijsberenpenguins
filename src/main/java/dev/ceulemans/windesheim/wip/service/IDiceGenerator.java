package dev.ceulemans.windesheim.wip.service;

import dev.ceulemans.windesheim.wip.exception.DiceGenerationException;
import dev.ceulemans.windesheim.wip.model.DiceResult;

public interface IDiceGenerator {

    /**
     * Generate a dice result with <code>amount</code> of dice.
     *
     * @param amount amount of dice to generate
     * @return the dice result of the amount of dice
     * @throws DiceGenerationException when amount is out of bounds
     */
    DiceResult generate(int amount) throws DiceGenerationException;
}
