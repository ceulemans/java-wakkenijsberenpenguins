package dev.ceulemans.windesheim.wip.service;

import dev.ceulemans.windesheim.wip.exception.DiceGenerationException;
import dev.ceulemans.windesheim.wip.model.DiceResult;
import dev.ceulemans.windesheim.wip.model.Die;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Service that is used to generate a certain amount of dice.
 */
public class DiceGenerator implements IDiceGenerator {

    private static final Logger logger = LoggerFactory.getLogger(DiceGenerator.class);

    /**
     * Generate a dice result with <code>amount</code> of dice.
     *
     * @param amount amount of dice to generate
     * @return the dice result of the amount of dice
     * @throws DiceGenerationException when amount is out of bounds
     */
    public DiceResult generate(int amount) throws DiceGenerationException {
        if (amount < 3 || amount > 8) {
            throw new DiceGenerationException();
        }

        List<Die> dice = new ArrayList<>();
        Random random = new Random();
        Die[] values = Die.values();

        for (int i = 0; i < amount; i++) {
            dice.add(values[random.nextInt(values.length)]);
        }

        logger.debug("Generated dice: {}", dice.stream().map(Die::toString).collect(Collectors.joining(", ")));

        return DiceResult.of(dice);
    }
}
